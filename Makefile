.PHONY: build

build:
	docker build --pull -t forwardaws/php:$(shell git rev-parse HEAD) .
	docker tag forwardaws/php:$(shell git rev-parse HEAD) forwardaws/php:$(shell git rev-parse --abbrev-ref HEAD)
	docker push forwardaws/php:$(shell git rev-parse HEAD)
	docker push forwardaws/php:$(shell git rev-parse --abbrev-ref HEAD)
